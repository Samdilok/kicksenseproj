﻿using System;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using System.IO.Ports;
using System.Threading;
using System.IO;

namespace ForceMoniApp_V2
{
    public partial class RealtimeForceMonitoring : Form
    {
        private Thread readInputThread;
        private SerialPort myport;
        private string in_data;
        private string[] split_data = new string[2];
        private Boolean PortState = false;
        private double KickTotal = 0;

        public RealtimeForceMonitoring()
        {
            InitializeComponent();
            Connect_btn.Enabled = true;
            Disconnect_btn.Enabled = false;
            ComPort_set.Text = "COM31";
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void getDatafromPort()
        {
            while (true)
            {
                if (PortState)
                {
                    try
                    {
                        in_data = myport.ReadLine();
                        split_data = in_data.Split(',');
                        
                        

                        //Data_in.Text = in_data;
                        this.Invoke((MethodInvoker)delegate { displaydata(); });
                    }
                    catch (System.IO.IOException error)
                    {
                        return;
                    }
                    catch (System.InvalidOperationException error)
                    {
                        return;
                    }
                }
                //Thread.Sleep(1);
            }

        }
        private void Connect_btn_Click_1(object sender, EventArgs e)
        {
            myport = new SerialPort();
            myport.BaudRate = 38400;
            myport.PortName = ComPort_set.Text;
            myport.Parity = Parity.None;
            myport.DataBits = 8;
            myport.StopBits = StopBits.One;
            //myport.DataReceived += Myport_DataReceived;
            try
            {
                myport.Open();

                PortState = true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Please connect device or check COM_PORT name");
            }
            Connect_btn.Enabled = false;
            Disconnect_btn.Enabled = true;

            readInputThread = new Thread(new ThreadStart(this.getDatafromPort));
            readInputThread.IsBackground = true;
            readInputThread.Start();

        }

        private void displaydata()
        {
            //Console.WriteLine(in_data);
            //Data_in.Text = ch1_subarray_data.Length.ToString();
            split_data = in_data.Split(',');
            KickTotal = KickTotal + Convert.ToDouble(split_data[1]);
            CountKick.Text = KickTotal.ToString();
            /*if (split_data.Length == sample_to_read)
            {
                
                int[] subarray_data = Array.ConvertAll(split_data, int.Parse);              //array of string is converted to arrya of integer
                Data_in.Text = Math.Floor(subarray_data.Average()).ToString();                     // show average data over 10 points
                //Array.Copy(array_data, 10, array_data, 0, array_data.Length - 10);
                Array.Copy(subarray_data, 0, array_data, index_graph_array, subarray_data.Length); //pad subarray to main array
                                                                                                                        //pad main array itself with 10 point translation
                                                                                                                        //array_data[array_data.Length - 1] = subarray_data[0];
                                                                                                                        //Array.Copy(array_data, 1, array_data, 0, array_data.Length - 1);
                index_graph_array += sample_to_read;

                int positionX = (890 * index_graph_array) / array_data.Length;
                cursor_Y_axis.Location = new Point(positionX + 212, 194); //label cursor location : start 212,193 : stop 1090, 193 : graph location: 83,176

                if (index_graph_array >= array_data.Length)
                {
                    index_graph_array = 0;
                }

                int max_data_now = array_data.Max();
                if (max_data_now > 100)
                {
                    in_data_graph.ChartAreas[0].AxisY.Maximum = max_data_now;
                } else
                {
                    in_data_graph.ChartAreas[0].AxisY.Maximum = 100;
                }
                

                in_data_graph.Series["Force"].Points.Clear();
                for (int i = 0; i < array_data.Length - 1; i = i + scaling_factor)
                {
                    in_data_graph.Series["Force"].Points.AddY(array_data[i]);                   //plot graph
                }

                if (flag_max == false)
                {
                    max_data = max_data_now;
                    max_data_tb.Text = max_data.ToString();
                }

                if (flag_start)
                {
                    index_count = index_count + 1;
                    if (index_count >= (array_data.Length / 10))
                    {
                        Array.Resize(ref array_data_save, array_data.Length);
                        Array.Copy(array_data, 0, array_data_save, 0, array_data.Length);
                        for (int j = 0; j < array_data_save.Length - 1; j = j + scaling_factor)
                        {
                            save_graph.Series["Force"].Points.AddY(array_data_save[j]);
                            save_graph.Series["Threshold"].Points.AddY(max_data);
                        }
                        for (int k = 0; k < array_data_save.Length - 1; k++)
                        {
                            data_save += array_data_save[k];
                            data_save += Environment.NewLine;
                        }
                        index_count = 0;
                        flag_start = false;
                        Save_btn.Enabled = true;
                        //check_tb.Text = data_save;
                        X_max2.Show();
                        X_Time2.Show();
                    }

                }

            }
            */
        }

        private void Disconnect_btn_Click_1(object sender, EventArgs e)
        {
            PortState = false;
            try
            {
                
                myport.Close();
                
            }
            catch (Exception ex1)
            {
                MessageBox.Show(ex1.Message, "Cannot close COM PORT");
            }
            Connect_btn.Enabled = true;
            Disconnect_btn.Enabled = false;

            readInputThread.Abort();

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void Reset_btn_Click(object sender, EventArgs e)
        {
            KickTotal = 0;
            CountKick.Text = KickTotal.ToString();
        }
    }
}
