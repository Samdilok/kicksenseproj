﻿namespace ForceMoniApp_V2
{
    partial class RealtimeForceMonitoring
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.ComPort_set = new System.Windows.Forms.TextBox();
            this.Disconnect_btn = new System.Windows.Forms.Button();
            this.Connect_btn = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.CountKick = new System.Windows.Forms.TextBox();
            this.Reset_btn = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label1.Location = new System.Drawing.Point(895, 30);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(116, 24);
            this.label1.TabIndex = 20;
            this.label1.Text = "COM PORT:";
            // 
            // ComPort_set
            // 
            this.ComPort_set.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.ComPort_set.Location = new System.Drawing.Point(1016, 27);
            this.ComPort_set.Name = "ComPort_set";
            this.ComPort_set.Size = new System.Drawing.Size(85, 29);
            this.ComPort_set.TabIndex = 19;
            // 
            // Disconnect_btn
            // 
            this.Disconnect_btn.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.Disconnect_btn.Location = new System.Drawing.Point(1212, 22);
            this.Disconnect_btn.Name = "Disconnect_btn";
            this.Disconnect_btn.Size = new System.Drawing.Size(112, 38);
            this.Disconnect_btn.TabIndex = 18;
            this.Disconnect_btn.Text = "Disconnect";
            this.Disconnect_btn.UseVisualStyleBackColor = true;
            this.Disconnect_btn.Click += new System.EventHandler(this.Disconnect_btn_Click_1);
            // 
            // Connect_btn
            // 
            this.Connect_btn.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.Connect_btn.Location = new System.Drawing.Point(1107, 22);
            this.Connect_btn.Name = "Connect_btn";
            this.Connect_btn.Size = new System.Drawing.Size(99, 38);
            this.Connect_btn.TabIndex = 17;
            this.Connect_btn.Text = "Connect";
            this.Connect_btn.UseVisualStyleBackColor = true;
            this.Connect_btn.Click += new System.EventHandler(this.Connect_btn_Click_1);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label2.Location = new System.Drawing.Point(562, 82);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(305, 42);
            this.label2.TabIndex = 22;
            this.label2.Text = "KickSense Demo";
            this.label2.Click += new System.EventHandler(this.label2_Click);
            // 
            // CountKick
            // 
            this.CountKick.Font = new System.Drawing.Font("Microsoft Sans Serif", 72F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.CountKick.Location = new System.Drawing.Point(555, 221);
            this.CountKick.Name = "CountKick";
            this.CountKick.Size = new System.Drawing.Size(322, 116);
            this.CountKick.TabIndex = 23;
            // 
            // Reset_btn
            // 
            this.Reset_btn.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.Reset_btn.Location = new System.Drawing.Point(650, 370);
            this.Reset_btn.Name = "Reset_btn";
            this.Reset_btn.Size = new System.Drawing.Size(133, 44);
            this.Reset_btn.TabIndex = 24;
            this.Reset_btn.Text = "Reset";
            this.Reset_btn.UseVisualStyleBackColor = true;
            this.Reset_btn.Click += new System.EventHandler(this.Reset_btn_Click);
            // 
            // RealtimeForceMonitoring
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1350, 729);
            this.Controls.Add(this.Reset_btn);
            this.Controls.Add(this.CountKick);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.ComPort_set);
            this.Controls.Add(this.Disconnect_btn);
            this.Controls.Add(this.Connect_btn);
            this.Name = "RealtimeForceMonitoring";
            this.Text = "Real Time Force Monitoring";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox ComPort_set;
        private System.Windows.Forms.Button Disconnect_btn;
        private System.Windows.Forms.Button Connect_btn;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox CountKick;
        private System.Windows.Forms.Button Reset_btn;
    }
}

